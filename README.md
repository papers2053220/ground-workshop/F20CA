<!-- Improved compatibility of back to top link: See: https://github.com/othneildrew/Best-README-Template/pull/73 -->
<a name="readme-top"></a>



<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
<!-- [![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url] -->



<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://github.com/Zein2002/F20CA">
    <img src="https://images.pexels.com/photos/2085831/pexels-photo-2085831.jpeg" alt="Logo" width="120" height="80">
  </a>

<h3 align="center">F20CA - Conversational Agents and Spoken Language Processing</h3>

  <p align="center">
    A multi-party spoken dialogue system that plays "Who wants to be a millionaire?" with you. 
    <br />
    <br />
    <a href="https://github.com/Zein2002/F20CA/issues">Report Bug</a>
    ·
    <a href="https://github.com/Zein2002/F20CA/issues">Request Feature</a>
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li><a href="#about-the-project">About The Project</a></li>
    <li><a href="#group_members">Group Members</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>
<br>



<!-- ABOUT THE PROJECT -->
## About The Project

This Repository contains our exploration code for the F20CA group project. The aim of the project is to create a spoken dialogue system that can handle multi-party conversations. Our system took inspiration from "Who wants to be a millionaire?" and can play the famous question-answer game with up to two people.


<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- GROUP MEMBERS -->
## Group Members 

| Name           | E-Mail | Role                           |
| :------------- | :----: | :----------------------------- |
| Jason Sweeney  | js418  | Manager                        |
| Laura Schauer  |  lms9  | Literature & Report            |
| Charlie Lyttle | cl157  | ASR (Speech Recognition)       |
| Àron Szeles    | as472  | Data Collection & Evaluation   |
| Katie McAskill | klm12  | Data Collection & Evaluation   |
| Zein Said      | zs2008 | Natural Language Understanding |
| Abdullah Sayed | as512  | Natural Language Understanding |
| Cale Clark     | cc164  | Dialogue Manager               |
| Xander Wickham | aw127  | Dialogue Manager               |
| Tom Byars      | tjb10  | Natural Language Generation    |
| Roman Fenlon   | rf104  | Natural Language Generation    |
| Jack Miller    |  jjm7  | System Integration             |

<br>


<!-- ROADMAP -->
## Roadmap

![image](https://media.discordapp.net/attachments/1067033040028704771/1073627793914613760/image.png?width=1440&height=728)

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- ACKNOWLEDGMENTS -->
## Acknowledgments

Our supervisors: 

* Lemon, Oliver; o.lemon  
* Gunson, Nancie; n.gunson  
* Addlesee, Angus; angus.addlesee  
* Hernandez Garcia, Daniel; d.hernandez_garcia  

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/github/contributors/Zein2002/F20CA.svg?style=for-the-badge
[contributors-url]: https://github.com/Zein2002/F20CA/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/Zein2002/F20CA.svg?style=for-the-badge
[forks-url]: https://github.com/Zein2002/F20CA/forks
[stars-shield]: https://img.shields.io/github/stars/Zein2002/F20CA.svg?style=for-the-badge
[stars-url]: https://github.com/Zein2002/F20CA/stargazers
[issues-shield]: https://img.shields.io/github/issues/Zein2002/F20CA.svg?style=for-the-badge
[issues-url]: https://github.com/Zein2002/F20CA/issues
[product-screenshot]: https://images.pexels.com/photos/2085831/pexels-photo-2085831.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1